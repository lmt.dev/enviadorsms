﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Discados.aspx.cs" Inherits="EnviadorSMS.Discados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
          <h1 class="h3 mb-2 text-gray-800">Panel de Discados
    </h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">


                          
                    <asp:UpdatePanel ID="upGridViewSMSDiscados" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:GridView ID="gvSMSDiscados" CssClass="table table-striped table-bordered table-hover" data-model="SMS.aspx"
                                Width="100%" AutoGenerateColumns="False" runat="server" 
                                EmptyDataText="Sin datos para mostrar." AllowPaging="True"
                                OnPageIndexChanging="gvSMSDiscados_PageIndexChanging" PageSize="100"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="# Discado" DataField="DiscadoId" />
                                    <asp:BoundField HeaderText="Celular" DataField="DiscadoCelular" />
                                    <asp:BoundField HeaderText="Fecha Creado" DataField="FHCreado" ItemStyle-CssClass="date-time" />
                                    <asp:BoundField HeaderText="Fecha Discado" DataField="FHDiscado" ItemStyle-CssClass="date-time" />
                                    <asp:BoundField HeaderText="Terminal" DataField="TerminalNombre" />
                                    <asp:BoundField HeaderText="Version Soft" DataField="VersionNombre" />
                                    <asp:BoundField HeaderText="Campaña" DataField="CampanaNombre" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

              </div>
            </div>
          </div>
</asp:Content>
