import logging
import os
import sys
import win32api
from multiprocessing import Process
from time import sleep

import serial.tools.list_ports

from Config import PID_FILE_FOLDER, ENVIAR_SMS_CADA, LEER_MENSAJES_CADA
from objetos.HuaweiModem import HuaweiModem as HM

test=False

logger = logging.getLogger(__name__)
HuaweiModem = HM

def BuscarModems():
    ports = serial.tools.list_ports.comports()
    rta = []
    for port, desc, hwid in sorted(ports):
        if "PC UI" in desc:
            rta.append(int(port.replace("COM","")-1))
    return rta





def loop_principal(puerto):
    try:
        class ContextFilter(logging.Filter):
            """
            This is a filter which injects contextual information into the log.
            """

            def filter(self, record):
                record.PUERTO = puerto
                return True

        formatter = logging.Formatter('%(PUERTO)s %(asctime)s %(levelname)-8s %(message)s')

        logger.setLevel(logging.DEBUG)
        logger.addFilter(ContextFilter())

        handler_file = logging.FileHandler('logs/'+puerto+'_log.log', mode='a', encoding='utf-8')
        handler_file.setFormatter(formatter)
        logger.addHandler(handler_file)

        handler_stream = logging.StreamHandler()
        handler_stream.setFormatter(formatter)
        logger.addHandler(handler_stream)
        logger.info('iniciando')
        bot = HuaweiModem(puerto,logger,debug=True)
        enviar_sms_cada = ENVIAR_SMS_CADA
        leer_sms_cada = LEER_MENSAJES_CADA
        logger.debug("enviar_sms_cada " + str(enviar_sms_cada))
        logger.debug("leer_sms_cada " + str(leer_sms_cada))
        contador_de_segundos = 0
        envios_fallidos = 0
        while True:
            if envios_fallidos > 10:
                logger.error("limite maximo de envios fallidos superado")
                break
            borrar=[]
            sleep(1)
            contador_de_segundos = contador_de_segundos + 1
            if contador_de_segundos % leer_sms_cada == 0:
                logger.info(str(bot.GetSIGNAL()))
                #leer recibidos
                mensajes = bot.GetAllSMS()
                if mensajes:
                    logger.info(str(len(mensajes))+" mensajes en la casilla")
                for mensaje in mensajes:
                    if "SMS-DELIVER" in mensaje.tipo: #sms
                        #insertar recibido
                        logger.info("sms recibido "+mensaje.origen)
                        if "ping" in mensaje.cuerpo.lower():
                            bot.SendSMS(mensaje.origen,"pong")
                        elif "/enviara" in mensaje.cuerpo.lower():
                            _destino = mensaje.cuerpo.split(" ")[1]
                            _mensaje = " ".join(mensaje.cuerpo.split(" ")[2:])
                            bot.SendSMS(_destino,_mensaje)
                        else:
                            bot.InsertarRecibido(mensaje.origen, mensaje.cuerpo)
                        borrar.append(mensaje.inbox_id)


                    if "SMS-STATUS-REPORT" in mensaje.tipo: #es Delivery Report
                        #insertar entregado
                        logger.info("reporte recibido "+mensaje.destino)
                        bot.InsertarEntregado(mensaje.destino)
                        borrar.append(mensaje.inbox_id)




                if borrar:
                    for mensaje_id in borrar:
                        logger.info("borrando "+ mensaje_id)
                        bot.ClearSMS(mensaje_id)

            if contador_de_segundos % enviar_sms_cada == 0:
                if test:
                    resultado = bot.SendSMS("321",".")

                else:
                    #obtener discado
                    discado = bot.ObtenerNumeroDiscado()
                    speech = discado['speechDiscar']
                    numero = discado['celularDiscar']
                    if not speech is None:
                        
                        logger.info("enviar sms a " + numero)
                        resultado = bot.SendSMS(discado['celularDiscar'],speech)
                        logger.info(resultado)
                        if "timeout" in resultado:
                            print("envio fallido")
                            envios_fallidos = envios_fallidos + 1
                        else:
                            try:
                                if resultado[1] is 'OK':
                                    logger.info("SMS enviado a" + numero)
                            except Exception:
                                if "ERROR: 500" in resultado[0]:
                                    logger.info("SMS NO enviado a" + numero)
                    else:
                        logger.info("Sin datos")

            if contador_de_segundos >= enviar_sms_cada * leer_sms_cada:
                contador_de_segundos = 0
    except KeyboardInterrupt:
        # quit
        pass
    except Exception as e:
        logger.exception(e)
        logger.exception("el sueño termino")


def main(PUERTO_COM):
    filename = PUERTO_COM.replace("COM","COM_")
    open(PID_FILE_FOLDER+"/"+filename, "a").close()
    proc = Process(target=loop_principal, args=(PUERTO_COM,))
    proc.start()
    sleep(1)
    try:
        proc.join()
    except KeyboardInterrupt:
        pass
    os.remove(PID_FILE_FOLDER+"/"+filename)
    print("fin del main")

def on_exit(sig, func=None):
    filename = PUERTO_COM.replace("COM", "COM_")
    print("exit handler")
    os.remove(PID_FILE_FOLDER + "/" + filename)

if __name__ == '__main__':
    win32api.SetConsoleCtrlHandler(on_exit, True)
    PUERTO_COM = sys.argv[1]
    main(PUERTO_COM)



if __name__ == '__main__2':
    win32api.SetConsoleCtrlHandler(on_exit, True)
    PUERTO_COM=None
    try:
        arg = sys.argv[1]
        if arg is "test":
            test =True
    except:
        pass
    os.system('cls')
    usb_modems = BuscarModems()
    string = ""
    index = 0
    opciones = []
    for modem in usb_modems:
        andando = ""
        file = modem.replace("COM", "COM_")
        pid_file = os.path.join(PID_FILE_FOLDER + "/" + file)
        if os.path.isfile(pid_file):
            andando = "_*"
            # puerto ocupado
        string = string + ""+modem+andando+"["+str(index)+"] "
        opciones.append(index)
        index = index + 1
    puerto_index = -1
    while int(puerto_index) not in opciones:
        puerto_index=input("Elegir entre puertos: "+string)
    PUERTO_COM = usb_modems[int(puerto_index)]
    main(PUERTO_COM)






