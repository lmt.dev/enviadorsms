import logging
from time import sleep

import serial
import binascii

import zeep

from Config import WSDL
from objetos.ObjDeliveryReport import ObjDeliveryreport
from objetos.ObjSMS import ObjSMS
from objetos.pdu import decodeSmsPdu, encodeSmsSubmitPdu


def pairwise(iterable):
    a = iter(iterable)
    return zip(a, a)


class HuaweiModem():
    puerto_com = "COMX"
    nombre = ""
    IMEI = ""
    debug = False
    ICCID = ""
    logger = None


    def __init__(self, port,logger,debug = False):
        self.logger = logger
        self.nombre = port
        self.puerto_com = port
        self.debug = debug
        self.open()

    def open(self):
        self.ser = serial.Serial(self.puerto_com, 115200, timeout=0)
        self.SendCommand('AT\r')
        self.SendCommand('AT+CMGF=0\r')
        self.SendCommand('AT+CSCS="GSM"\r')
        self.IMEI = self.GetIMEI()
        self.ICCID = self.GetICCID()
        #self.SendCommand('AT+CSMP=49,167,0,0\r') #request delivery report text mode


    def SendCommand(self,command, getline=True,encode=True,read_method=0,filter_strings=[],decode_rta=False):
        if encode:
            self.ser.write(command.encode())
            self.logger.debug("SendCommand() " + command.replace("\r",""))
        else:
            self.ser.write(command)
            self.logger.debug("SendCommand() " + command.decode().replace("\r",""))

        data = ''
        if getline:
            if read_method == 0:
                data=self.ReadLines()
            elif read_method == 1:
                self.ser.timeout = 2
                data = self.ser.readall()
                self.logger.debug("ReadLines2() "+" ".join(data))
        return data

    def ReadLines(self):
        data = ''
        timeout = 0
        while True:
            if timeout > 15:
                data = "TIMEOUT"
                break
            timeout = timeout + 1
            undecoded_data = self.ser.readall()
            self.logger.debug("ReadLines() " + undecoded_data.decode().replace("\n", "").replace("\t", "").replace("\r", ""))
            data = data + undecoded_data.decode()
            if '\r\nOK\r\n' in data:
                break
            if '\r\n> ' in data:
                break
            if '\r\n+CMS ERROR:' in data:
                break
            if "+CDSI:" in data:
                break
            if "+CSQ:" in data:
                break
            sleep(1)
        data = [d for d in data.splitlines() if d and not "^RSSI:" in d and len(d) > 0]

        return data

    def ReadLines2(self,decode_rta=False,fs=[]):
        def decodificador(t):
            try:
                if not decode_rta:
                    raise Exception("")
                return binascii.unhexlify(t).decode('utf-16-be')
            except:
                return t
        self.ser.timeout = 2
        data = ''
        while True:
            undecoded_data = self.ser.readall()
            self.logger.debug("ReadLines() " + undecoded_data.decode().replace("\n","").replace("\t","").replace("\r","") )
            data = data + undecoded_data.decode()
            if '\r\nOK\r\n' in data:
                break
            if '\r\n> ' in data:
                break
            if '\r\n+CMS ERROR:' in data:
                break
            sleep(1)

        for s in fs:
            data = data.replace(s,"")
        data = [decodificador(d) for d in data.splitlines() if d and not "^RSSI:" in d and len(d)>0]
        return data




    def GetAllSMS(self):
        rta = []
        self.ser.flushInput()
        self.ser.flushOutput()
        self.logger.debug("GetAllSMS()")
        data = self.SendCommand('AT+CMGL=4\r')

        data = [d for d in data if not "MODE:" in d and not "OK" in d and not "CMTI" in d]

        for raw_cabecera, raw_pdu in pairwise(data):
            if "CMGL:" in raw_cabecera:
                #cabecera detectada
                cabecera = raw_cabecera.replace("+CMGL: ", "").split(",")
                mensaje_id = cabecera[0].replace('CMGL: ','').replace("'","").replace('"','')
                try:
                    mensaje_pdu = decodeSmsPdu(raw_pdu)
                except Exception as e:
                    print("error",raw_pdu)
                    print(e)
                tipo = mensaje_pdu.get("type")
                if "SMS-DELIVER" in tipo: #es sms
                    #tipo = "SMS"
                    sms = ObjSMS(mensaje_id, mensaje_pdu.get("text"),tipo)
                    sms.origen =  mensaje_pdu.get("number") #de donde viene el sms
                    rta.append(sms)
                elif "SMS-STATUS-REPORT" in tipo:
                    #delivery report
                    delivery_report = ObjDeliveryreport(mensaje_id, mensaje_pdu.get("number"),tipo)
                    rta.append(delivery_report)
        return rta


    def SendSMS(self,numero,mensaje):
        self.ser.flushInput()
        self.ser.flushOutput()
        self.logger.debug("SendSMS() "+numero)
        pdu = encodeSmsSubmitPdu(numero, mensaje)[0]
        pdu = str(pdu)
        octetos = str(len(pdu)/2 - 1).split(".")[0]
        print("octetos",octetos)
        self.SendCommand('AT+CMGS=' + str(octetos)+"\r")
        rta = self.SendCommand(pdu.encode()+bytes([26]),encode=False)
        return rta

    def SendSMSTEXT(self,numero,mensaje):
        self.ser.flushInput()
        self.ser.flushOutput()
        self.logger.debug("SendSMS() "+numero)
        #numero = "3512829763"
        #mensaje ="49,167,0,0"
        self.SendCommand('AT+CMGS="' + numero + '"\r')
        self.SendCommand(mensaje + "\r")
        rta = self.SendCommand(bytes([26]), encode=False)
        return rta

    def ClearSMS(self,_sms_id):
        self.logger.debug("ClearSMS() "+_sms_id)
        self.ser.flushInput()
        self.ser.flushOutput()
        self.SendCommand('AT+CMGD='+_sms_id+'\r',getline=False)

    def ClearAllSMS(self):
        self.logger.debug("ClearAllSMS() ")
        self.ser.flushInput()
        self.ser.flushOutput()
        self.SendCommand('AT+CMGD=1,4\r',getline=False)

    def GetIMEI(self):
        self.logger.debug("GetIMEI() ")
        self.ser.flushInput()
        self.ser.flushOutput()
        while True:
            rta = self.SendCommand('AT+CGSN\r')
            self.IMEI = rta
            if not rta[0].upper() is 'OK':
                break
            else:
                sleep(5)
                self.logger.debug("IMEI INVALIDO")

        return rta[0]

    def GetICCID (self):
        self.logger.debug("GetICCID() ")

        self.ser.flushInput()
        self.ser.flushOutput()
        rta = self.SendCommand('AT^ICCID?\r',filter_strings=["^ICCID: "])
        return rta[0].replace("^ICCID: ", "")


    def GetSIGNAL(self):
        self.logger.debug("GetSIGNAL() ")

        self.ser.flushInput()
        self.ser.flushOutput()
        rta = self.SendCommand('AT+CSQ\r')
        sleep(0.5)
        rta = [n for n in rta if "+CSQ:" in n]
        a = {2:"-109",3:"-107",4:"-105",5:"-103",6:"-101",7:"-99",8:"-97",9:"-95",10:"-93",11:"-91",12:"-89",13:"-87",14:"-85",15:"-83",16:"-81",17:"-79",18:"-77",19:"-75",20:"-73",21:"-71",22:"-69",23:"-67",24:"-65",25:"-63",26:"-61",27:"-59",28:"-57",29:"-55",30:"-53"}
        index = rta[0].replace("+CSQ: ","").split(",")[0]
        return a[int(index)] + " dbm"

    def ObtenerNumeroDiscado(self):
        wsdl = WSDL
        client = zeep.Client(wsdl=wsdl)
        pIMEI = self.IMEI
        pNroSerieSim = self.ICCID
        respuesta = client.service.ObtenerNumeroDiscado(pIMEI,pNroSerieSim)
        odict = zeep.helpers.serialize_object(respuesta)
        self.logger.debug("ObtenerNumeroDiscado() ")
        return odict

    def InsertarRecibido(self,_celular, _mensaje):
        wsdl = WSDL
        client = zeep.Client(wsdl=wsdl)
        pIMEI = self.IMEI
        pNroSerieSim = self.ICCID
        self.logger.debug("_celular: "+_celular +" _mensaje: "+ _mensaje+" pIMEI: "+ pIMEI+" pNroSerieSim: "+pNroSerieSim)
        respuesta = client.service.InsertarRecibido(_celular, _mensaje, pIMEI,pNroSerieSim)
        odict = zeep.helpers.serialize_object(respuesta)
        self.logger.debug("InsertarRecibido() ")
        return odict

    def InsertarEntregado(self,_celular):
        wsdl = WSDL
        client = zeep.Client(wsdl=wsdl)
        pIMEI = self.IMEI
        pNroSerieSim = self.ICCID
        respuesta = client.service.InsertarEntregado(pIMEI,_celular)
        odict = zeep.helpers.serialize_object(respuesta)
        self.logger.debug("InsertarEntregado()")
        return odict










