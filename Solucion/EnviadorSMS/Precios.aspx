﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Precios.aspx.cs" Inherits="EnviadorSMS.Precios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="h3 mb-2 text-gray-800">Envios masivos de SMS.
    </h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-info">Precios por campañas. (Pesos Argentinos (ARS))</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card border-left-default shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary mb-1">Plan 'Conóceme'. Paso 1: Regístrese. Paso 2: Cree su campaña, es múy fácil, sólo debe ingresar su 'Speech', seleccione la localidad a donde desee enviarlo y nosotros haremos el resto!. Utilice todas las funcionalidades disponibles para nuestros usuarios con licencia 100% GRATIS. Contáctese con nuestros representantes. Soporte 24/7</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">$0,000</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Enviar 1000 (Mil) SMS GRATIS!</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-bicycle fa-2x text-gray-300"></i>
                        </div>
                        </div>

                        <div class="btn-models">
                            <a href="Registro.aspx" class="btn btn-primary">
                                <asp:Label Text="Regístrese en minutos y comience a enviar su campaña gratuita! Consiga regalos especiales a través de nuestros resellers!" runat="server" />
                            </a>
                        </div>
                    </div>
                    </div>

                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary mb-1">Plan 'Urbano'. Envíe 3000 (Tres mil) SMS al precio de $0.25 cada uno.</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">$0,25 x SMS</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Enviar 3000 (Tres mil) SMS a $750!</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-bus fa-2x text-gray-300"></i>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary mb-1">Plan 'VIP'. Envíe 5000 (Cinco mil) SMS al precio de $0.20 cada uno.</div>

                            <div class="h5 mb-0 font-weight-bold text-gray-800">Enviar 5000 (Cinco mil) SMS a $1000!</div>
                            <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20% de descuento</div>
                            </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-taxi fa-2x text-gray-300"></i>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary mb-1">Plan 'Despegue'. Envíe 10000 (Diez mil) SMS al precio de $0.15 cada uno. </div>

                            <div class="h5 mb-0 font-weight-bold text-gray-800">Enviar 10000 (Diez mil) SMS a $1500!</div>
                            <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">40% de descuento</div>
                            </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                    <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-car fa-2x text-gray-300"></i>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary mb-1">Plan 'A fondo'. Envíe 50000 (Cincuenta mil) SMS al precio de $0.10 cada uno.</div>

                            <div class="h5 mb-0 font-weight-bold text-gray-800">Enviar 50000 (Cincuenta mil) SMS a $5000!</div>
                            <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">70% de descuento</div>
                            </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-rocket fa-2x text-gray-300"></i>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary mb-1">Plan 'Imparable'. Envíe 100000 (Cien mil) SMS al precio de $0.075 cada uno.</div>

                            <div class="h5 mb-0 font-weight-bold text-gray-800">Enviar 100000 (Cien mil) SMS a $7500!</div>
                            <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">80% de descuento</div>
                            </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-space-shuttle fa-2x text-gray-300"></i>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

              </div>
            </div>
          </div>
</asp:Content>
