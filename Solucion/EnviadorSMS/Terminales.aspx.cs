﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Persistencia;

namespace EnviadorSMS
{
    public partial class Terminales : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarTerminales();
        }

        protected void cargarTerminales()
        {
            DiscadoDatos discadoDatos = new DiscadoDatos();
            gvTerminales.DataSource = discadoDatos.ObtenerTerminales();
            gvTerminales.DataBind();
            upGridViewTerminales.Update();
        }

        protected void GridViewTerminales_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvTerminales.PageIndex = e.NewPageIndex;
                gvTerminales.DataBind();
                cargarTerminales();
            }
            catch (Exception)
            {
            }
        }
    }
}