﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Persistencia;

namespace EnviadorSMS
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private DataTable ObtenerRecibidos()
        {
            DataTable dt = new DataTable();
            DiscadoDatos discadoDatos = new DiscadoDatos();
            dt = discadoDatos.ObtenerRecibidos(1);
            return dt;
        }
    }
}