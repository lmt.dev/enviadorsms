﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Campanas.aspx.cs" Inherits="EnviadorSMS.Campanas" %>
<%@ Register Src="~/Controls/UCTraza.ascx" TagPrefix="uc" TagName="UCTraza" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
          <h1 class="h3 mb-2 text-gray-800">Panel de Campañas
    </h1>

    	        <asp:UpdatePanel ID="upErrorHandler" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		        <ContentTemplate>
			        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
				        <label>
					        <asp:Label ID="lblErrorHandler" Text="" runat="server" />
				        </label>
			        </div>
		        </ContentTemplate>
	        </asp:UpdatePanel>

              <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Testing manual de envío SMS</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                          
                    <asp:UpdatePanel ID="upTestingManual" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <div class="row">

                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <label>
                                            <asp:Label Text="Ingrese el teléfono celular: " runat="server" />
                                        </label>
                                      <asp:TextBox ID="txtCelularTesting" CssClass="form-control numeric-integer-positive" placeholder="Celular 10 digitos. Ej: '3516099788'" runat="server" TextMode= "SingleLine" />
                                  </div>

                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <label>
                                            <asp:Label Text="Seleccione su campaña: " runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlCampaña" CssClass="form-control select-single" Enabled="false" runat="server" />
                                  </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <label>
                                            <asp:Label Text="Verifique el celular antes de enviar" runat="server" />
                                        </label>
                                    <div>
                                        <asp:Button ID="btnEnvio" 
                                        Text="Enviar SMS" CssClass="btn btn-primary" Enabled="false" runat="server" OnClick="btnEnvio_Click"/>
                                    </div>

                                </div>

                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

              </div>
            </div>
          </div>

          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Campañas</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                          
                    <asp:UpdatePanel ID="upGridViewCampanas" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:GridView ID="gvCampanas" CssClass="table table-striped table-bordered table-hover" data-model="SMS.aspx"
                                Width="100%" AutoGenerateColumns="False" runat="server" 
                                EmptyDataText="Sin datos para mostrar."
                                OnPageIndexChanging="GridViewCampanas_PageIndexChanging" PageSize="25" AllowPaging="True"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="#" DataField="IdCampana" />
                                    <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" />
                                    <asp:BoundField HeaderText="Es Modulo Automatico" DataField="EsModuloAutomatico" /> 
                                    <asp:BoundField HeaderText="Localidad" DataField="Localidad" />
                                    <asp:BoundField HeaderText="FH Ultima Generacion" DataField="FHUltimaGeneracion" />
                                    <asp:BoundField HeaderText="Discado activo" DataField="DiscadoActivo" />
                                    <asp:BoundField HeaderText="SMS Disponibles" DataField="SMSDisponibles" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

              </div>
            </div>
          </div>

     <uc:UCTraza runat="server" ID="UCTraza1" />
</asp:Content>
