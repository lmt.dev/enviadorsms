﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Recibidos.aspx.cs" Inherits="EnviadorSMS.Recibidos" %>
<%@ Register Src="~/Controls/UCTraza.ascx" TagPrefix="uc" TagName="UCTraza" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="h3 mb-2 text-gray-800">Bandeja de entrada
    </h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

                <asp:UpdatePanel ID="upGridViewSMSRecibidos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>

                        <asp:GridView ID="gvSMSRecibidos" CssClass="table table-striped table-bordered table-hover" data-model="Recibidos.aspx"
                            Width="100%" AutoGenerateColumns="False" runat="server" 
                            EmptyDataText="Sin datos para mostrar."
                            OnPageIndexChanging="gvSMSRecibidos_PageIndexChanging" PageSize="50" AllowPaging="True"
                            OnSelectedIndexChanging="gvSMSRecibidos_SelectedIndexChanging"
                            OnRowDeleting="gvSMSRecibidos_RowDeleting"
                            >
                            <Columns>
                                <asp:BoundField HeaderText="#" DataField="RecibidoIdRecibido" ItemStyle-Width="50"/>
                                <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                <asp:BoundField HeaderText="Celular" DataField="RecibidoCelular" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Mensaje" DataField="RecibidoMensaje" ItemStyle-Width="200"/>
                                <asp:BoundField HeaderText="FH Recibido" DataField="RecibidoFechaAlta" ItemStyle-CssClass="date-time" ItemStyle-Width="50"/>
                                <asp:BoundField HeaderText="Campaña" DataField="Campaña" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Speech" DataField="Speech" ItemStyle-Width="200"/>
                                <asp:TemplateField HeaderText="Anular" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnCambiarEstado"
                                            runat="server"
                                            CssClass="btn btn-danger col-lg-2 col-md-2 col-sm-2 col-xs-2"
                                            CommandName="Select"
                                            PlaceHolder="Marcar como ANULADO"
                                            Style="margin-left: 5px;">
                                                <span aria-hidden="true" class="glyphicon glyphicon-play"></span>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Guardar" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnCambiarEstado2"
                                            runat="server"
                                            CssClass="btn btn-success col-lg-2 col-md-2 col-sm-2 col-xs-2"
                                            CommandName="Delete"
                                            PlaceHolder="Marcar como CONTACTADO"
                                            Style="margin-left: 5px;">
                                                <span aria-hidden="true" class="glyphicon glyphicon-play"></span>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
              </div>
            </div>
          </div>
    <uc:UCTraza runat="server" ID="UCTraza1" />
</asp:Content>
