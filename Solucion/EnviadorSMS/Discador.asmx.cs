﻿using System.Web.Services;
using System.Data;
using Persistencia;
using Modelo;

namespace EnviadorSMS
{
    /// <summary>
    /// Descripción breve de Discador
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class Discador : System.Web.Services.WebService
    {
        DiscadoDatos discadoDatos = new DiscadoDatos();

        [WebMethod]
        public Discar ObtenerNumeroDiscado(
            string pIMEI,  
            string pNroSerieSim
            )
        {
            Discar discarEsto = new Discar();
            DataTable dtDiscado = discadoDatos.ObtenerNumeroDiscado(
                pIMEI,
                pNroSerieSim
                );

            discarEsto.celularDiscar = string.Empty;
            discarEsto.speechDiscar = string.Empty;
                if (dtDiscado != null)
                {
                    foreach (DataRow dtRowDiscado in dtDiscado.Rows)
                    {
                        discarEsto.celularDiscar = dtRowDiscado[0].ToString();
                        discarEsto.speechDiscar = dtRowDiscado[1].ToString();
                        return discarEsto;
                    }
                }
            return discarEsto;
        }

        [WebMethod]
        public bool InsertarRecibido(
            string pCelular, 
            string pMensaje,
            string pIMEI,
            string pNroSerieSim
            )
        {
            bool dtDiscado = discadoDatos.InsertarRecibido(
                pCelular,
                pMensaje,
                pIMEI,
                pNroSerieSim
                );
            return dtDiscado;
        }

        [WebMethod]
        public bool InsertarEntregado(string pIMEI, string pCelular)
        {
            bool dtDiscado = discadoDatos.InsertarEntregado(pIMEI, pCelular);
            return dtDiscado;
        }
    }
}
