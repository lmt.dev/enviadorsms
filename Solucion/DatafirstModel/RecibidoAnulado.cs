﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace DatafirstModel
{
    public partial class RecibidoAnulado : ISUD<RecibidoAnulado>
    {
        public RecibidoAnulado() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdRecibidoAnulado { get; set; }

        public int IdRecibido { get; set; }

        public string Celular { get; set; }

        public string Mensaje { get; set; }

        public int? IdTerminal { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public int? IdRecibidoEstado { get; set; }

        public int? IdSim { get; set; }
    }
}