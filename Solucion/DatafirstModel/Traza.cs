﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace DatafirstModel
{
    public partial class Traza : ISUD<Traza>
    {
        public Traza() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdTraza { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public int Estado { get; set; }
    }
}