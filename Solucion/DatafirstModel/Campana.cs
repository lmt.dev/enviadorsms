﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace DatafirstModel
{
    public partial class Campana : ISUD<Campana>
    {
        public Campana() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdCampana { get; set; }

        public string Nombre { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public bool? EsModuloAutomatico { get; set; }

        public string Localidad { get; set; }

        public DateTime? FHUltimaGeneracion { get; set; }

        public bool? DiscadoActivo { get; set; }
    }
}