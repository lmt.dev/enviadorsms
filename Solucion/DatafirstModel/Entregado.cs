﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace DatafirstModel
{
    public partial class Entregado : ISUD<Entregado>
    {
        public Entregado() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdEntregado { get; set; }

        public string Celular { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public int? IdTerminal { get; set; }

        public int? IdSim { get; set; }
    }
}