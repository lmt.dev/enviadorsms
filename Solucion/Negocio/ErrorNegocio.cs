using System.Data;
using Modelo;
using Persistencia;
namespace Negocio
{
    public class ErrorNegocio
    {
        ErrorPersistencia _ErrorPersistencia = new ErrorPersistencia();
        public bool InsertarError(Error ErrorNegocio)
        {
            return _ErrorPersistencia.InsertarError(ErrorNegocio);
        }
        public bool ActualizarError(Error ErrorNegocio)
        {
            return _ErrorPersistencia.ActualizarError(ErrorNegocio);
        }
        public bool EliminarError(Error ErrorNegocio)
        {
            return _ErrorPersistencia.EliminarError(ErrorNegocio);
        }
        public DataTable ListarErrors(string pFiltro)
        {
            return _ErrorPersistencia.ListarError(pFiltro);
        }
        public Error ConsultarError(int IdError)
        {
            return _ErrorPersistencia.ConsultarError(IdError);
        }
    }
} 
