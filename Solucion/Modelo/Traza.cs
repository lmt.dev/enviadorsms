using System;


namespace Modelo
{
    public class Traza
    {
        private int id;
        private string nombre, descripcion;
        private DateTime fhAlta, fhBaja;
        private int estado;


        public int idTraza
        {
            get { return id; }
            set { id = value; }
        }


        public string nombreTraza
        {
            get { return nombre; }
            set { nombre = value; }
        }


        public string descripcionTraza
        {
            get { return descripcion; }
            set { descripcion = value; }
        }


        public DateTime fhAltaTraza
        {
            get { return fhAlta; }
            set { fhAlta = value; }
        }


        public DateTime fhBajaTraza
        {
            get { return fhBaja; }
            set { fhBaja = value; }
        }


        public int estadoTraza
        {
            get { return estado; }
            set { estado = value; }
        }


    }
}
